package com.nafis.osm.Utility;

public class StringHolder {

    public static String baseURL = "http://202.4.106.3/mess/api";
    public static String SendData = baseURL + "/save";
    public static String AllParkingData = baseURL + "/parking_info";
    public static String Registration = baseURL + "/registration_save";
    public static String Login = baseURL + "/login";
    public static String approveStatus = baseURL + "/update_approved";
    public static String bookAParking = baseURL + "/update_occupied";
}

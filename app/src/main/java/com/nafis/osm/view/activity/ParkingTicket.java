package com.nafis.osm.view.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jraska.falcon.Falcon;
import com.nafis.osm.R;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class ParkingTicket extends AppCompatActivity {

    TextView parking_date, parking_cost, parking_name, parking_number, parking_from, parking_to;
    ImageView takeScreenshot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_ticket);

        Initialization();
        setDatatoContent();
        takeScreenshot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeScreenshot();
                finish();
            }
        });

    }

    private void setDatatoContent() {
        parking_date.setText("DATE : " + getIntent().getStringExtra("time"));
        parking_cost.setText("UNPAID : " + getIntent().getStringExtra("cost") + " TAKA");
        parking_name.setText(getIntent().getStringExtra("name"));
        parking_number.setText(getIntent().getStringExtra("mobile"));



        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa", Locale.getDefault());
        parking_from.setText("FROM : "+dateFormat.format(new Date()));
        parking_to.setText("TO : "+dateFormat.format(new Date()));


    }

    private void Initialization() {
        takeScreenshot = findViewById(R.id.takeScreenshot);

        parking_date = findViewById(R.id.parking_date);
        parking_cost = findViewById(R.id.parking_cost);
        parking_name = findViewById(R.id.parking_name);
        parking_number = findViewById(R.id.parking_number);

        parking_from = findViewById(R.id.parking_from);
        parking_to = findViewById(R.id.parking_to);
    }


    public void takeScreenshot() {
        File screenshotFile = getScreenshotFile();

        Falcon.takeScreenshot(this, screenshotFile);

        String message = "Screenshot captured to " + screenshotFile.getAbsolutePath();
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();

        Uri uri = Uri.fromFile(screenshotFile);
        Intent scanFileIntent = new Intent(
                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri);
        sendBroadcast(scanFileIntent);
    }

    protected File getScreenshotFile() {
        File screenshotDirectory;
        try {
            screenshotDirectory = getScreenshotsDirectory(getApplicationContext());
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss.SSS", Locale.getDefault());

        String screenshotName = dateFormat.format(new Date()) + ".png";
        return new File(screenshotDirectory, screenshotName);
    }

    private static File getScreenshotsDirectory(Context context) throws IllegalAccessException {
        String dirName = "screenshots_" + context.getPackageName();

        File rootDir;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            rootDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        } else {
            rootDir = context.getDir("screens", MODE_PRIVATE);
        }

        File directory = new File(rootDir, dirName);

        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                throw new IllegalAccessException("Unable to create screenshot directory " + directory.getAbsolutePath());
            }
        }

        return directory;
    }
}
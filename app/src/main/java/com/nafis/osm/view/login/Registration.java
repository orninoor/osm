package com.nafis.osm.view.login;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.snackbar.Snackbar;
import com.nafis.osm.R;
import com.nafis.osm.Utility.StringHolder;
import com.nafis.osm.Utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Registration extends AppCompatActivity {
    Button registration;
    Toolbar mytoolbar;
    EditText name, email, phone, password, retypePassword;
    String $name, $email, $phone, $password, $retypePassword;
    int user_type = 3;
    private RadioGroup radioAccesslevel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        Initialization();

        setToolBar();
        registration = (Button) findViewById(R.id.signIn);
        radioAccesslevel = (RadioGroup) findViewById(R.id.radioGroup);

        radioAccesslevel.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.contributor) {

                    user_type = 2;


                } else if (checkedId == R.id.visitor) {

                    user_type = 3;


                }
            }

        });



        registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getAllData(view);

            }
        });
    }

    private void getAllData(View view) {


        $name = name.getText().toString();
        $email = email.getText().toString();
        $phone = phone.getText().toString();
        $password = password.getText().toString();
        $retypePassword = retypePassword.getText().toString();


        if (!$name.contentEquals("")) {

            if (!$email.contentEquals("")) {

                if (!$phone.contentEquals("")) {

                    if (!$password.contentEquals("")) {

                        if (!$retypePassword.contentEquals("")) {

                            if ($password.contentEquals($retypePassword)) {


                                new RegisterUser().execute();


                            } else {
                                Snackbar.make(view, "Passowrd  does'nt match", Snackbar.LENGTH_LONG)
                                        .setAction("OK", null).show();
                            }

                        } else {


                            Snackbar.make(view, "Re-Passowrd field is blank", Snackbar.LENGTH_LONG)
                                    .setAction("OK", null).show();
                        }

                    } else {


                        Snackbar.make(view, "Passowrd field is blank .", Snackbar.LENGTH_LONG)
                                .setAction("OK", null).show();

                    }

                } else {


                    Snackbar.make(view, "Phone field is blank .", Snackbar.LENGTH_LONG)
                            .setAction("OK", null).show();
                }

            } else {


                Snackbar.make(view, "Email field is blank .", Snackbar.LENGTH_LONG)
                        .setAction("OK", null).show();
            }

        } else {


            Snackbar.make(view, "Name field is blank .", Snackbar.LENGTH_LONG)
                    .setAction("OK", null).show();

        }


    }


    private class RegisterUser extends AsyncTask<String, Void, Void> {

        String jsonString = "";

        @Override
        protected Void doInBackground(String... strings) {


            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", $name);
                jsonObject.put("email", $email);
                jsonObject.put("contact", $phone);
                jsonObject.put("password", $password);
                jsonObject.put("address", "");
                jsonObject.put("user_type", user_type);


                jsonString = Utility.post(StringHolder.Registration, "" + jsonObject);


                Log.e("**** RESPOSE ", jsonString);

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                JSONObject jsonObject = new JSONObject(jsonString);

                if (jsonObject.optInt("status") == 200) {

                    new SweetAlertDialog(Registration.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Registered !")
                            .setContentText("Login to use the apps .")
                            .setConfirmText("Login")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    finish();
                                }
                            })
                            .setCancelButton("Cancel", new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            })
                            .show();

                } else {

                    Toast.makeText(getApplicationContext(), "Registration Failed !", Toast.LENGTH_LONG).show();


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    private void Initialization() {


        name = (EditText) findViewById(R.id.et_name);
        email = (EditText) findViewById(R.id.et_email);
        phone = (EditText) findViewById(R.id.et_phone);
        password = (EditText) findViewById(R.id.et_pass);
        retypePassword = (EditText) findViewById(R.id.et_re_pass);
    }


    private void setToolBar() {
        mytoolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Registration");


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }

}

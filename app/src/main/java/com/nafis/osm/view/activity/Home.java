package com.nafis.osm.view.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nafis.osm.R;
import com.nafis.osm.helper.GPSTracker;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;

public class Home extends AppCompatActivity {
    IMapController mapController;
    MapView map;
    Toolbar mytoolbar;
    private int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATTION = 10;
    private int MY_PERMISSIONS_REQUEST_ACCESS_COURSE_LOCATION = 11;
    private int MY_PERMISSIONS_REQUEST_CALL_RECEIVE = 13;
    double $_latitude = 0.0, $_longitude = 0.0;
    Button search_button, advertise_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        setContentView(R.layout.home_o);
      //  setToolBar();
        advertise_button = findViewById(R.id.advertise_parking);
        search_button = findViewById(R.id.search_parking);

        advertise_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "First TV", Toast.LENGTH_LONG).show();
            }
        });
        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Second TV", Toast.LENGTH_LONG).show();
            }
        });

        GetAllPermission();

        map = (MapView) findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        mapController = map.getController();
        mapController.setZoom(17.0);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        GPSTracker gps = new GPSTracker(this);
        if (gps.canGetLocation()) {

            $_latitude = gps.getLatitude();
            $_longitude = gps.getLongitude();


            CurrentLocationPin($_latitude, $_longitude);

            Log.e("Location ===> ", gps.getLatitude() + "," + gps.getLongitude());

        }

    }

    /*
     *
     * Parameter :  latitude , longitude
     * Return : Set the Initial pin on Current location
     *
     * */

    private void CurrentLocationPin(double $_latitude, double $_longitude) {
        final GeoPoint geoPoint = new GeoPoint($_latitude, $_longitude);
        mapController.setCenter(geoPoint);
        OverlayItem overlayItem = new OverlayItem("Parking Space", "", geoPoint);
        Drawable markerDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pin_home);
        overlayItem.setMarker(markerDrawable);

        ArrayList<OverlayItem> overlayItemArrayList = new ArrayList<>();
        overlayItemArrayList.add(overlayItem);

        ItemizedOverlay<OverlayItem> locationOverlay = new ItemizedIconOverlay<>(overlayItemArrayList, new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
            @Override
            public boolean onItemSingleTapUp(int i, OverlayItem overlayItem) {


                return true;
            }

            @Override
            public boolean onItemLongPress(int i, OverlayItem overlayItem) {

                Log.e("***** LONG TAP ****", " *** TAPPY LONG ***");
                return false;
            }
        }, getApplicationContext());


        map.getOverlays().add(locationOverlay);
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();

    }

    private void GetAllPermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATTION);


        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_ACCESS_COURSE_LOCATION);


        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CALL_PHONE},
                MY_PERMISSIONS_REQUEST_CALL_RECEIVE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATTION) {
        } else {
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }



    private void setToolBar() {
        mytoolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

}
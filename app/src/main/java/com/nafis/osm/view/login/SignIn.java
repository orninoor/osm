package com.nafis.osm.view.login;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.material.snackbar.Snackbar;
import com.nafis.osm.R;
import com.nafis.osm.Utility.StringHolder;
import com.nafis.osm.Utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

public class SignIn extends AppCompatActivity {
    private int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATTION = 10;
    private int MY_PERMISSIONS_REQUEST_ACCESS_COURSE_LOCATION = 11;
    private int MY_PERMISSIONS_REQUEST_CALL_RECEIVE = 13;
    Button loginBtn;
    TextView forget_pass;
    EditText email, password;
    String $email = "", $password = "";

    ImageView register_button;

    SharedPreferences prefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_);
        GetAllPermission();
        Initialization();


        if (prefs.getString("username", "").contentEquals("")) {

        } else {
            email.setText(prefs.getString("username", ""));
            password.setText(prefs.getString("password", ""));
        }

        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(SignIn.this, Registration.class));
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getAllData();

                new SignInAsyn().execute();

            }
        });

        forget_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignIn.this, Forgetpassword.class));
            }
        });

    }

    private void GetAllPermission() {

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATTION);


        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_ACCESS_COURSE_LOCATION);


        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CALL_PHONE},
                MY_PERMISSIONS_REQUEST_CALL_RECEIVE);
    }

    private void Initialization() {
        prefs = getSharedPreferences("UserData", MODE_PRIVATE);
        editor = prefs.edit();


        loginBtn = (Button) findViewById(R.id.loginBtn);
        email = (EditText) findViewById(R.id.login_emailid);
        password = (EditText) findViewById(R.id.login_password);
        forget_pass = findViewById(R.id.forget_pass);
        register_button = findViewById(R.id.register_button);

    }

    private void getAllData() {

        $email = email.getText().toString();
        $password = password.getText().toString();

    }


    private class SignInAsyn extends AsyncTask<String, Void, Void> {

        String jsonString;

        @Override
        protected Void doInBackground(String... strings) {

            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("email", $email);
                jsonObject.put("password", $password);
                jsonString = Utility.post(StringHolder.Login, "" + jsonObject);

                Log.e("$$$ Response :  ", jsonString);

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            try {
                JSONObject jsonObject = new JSONObject(jsonString);

                if (jsonObject.optInt("status") == 200) {


                    JSONObject nestedObject = jsonObject.getJSONObject("result");


                    editor.putString("username", nestedObject.optString("email"));
                    editor.putString("password", $password);
                    editor.putString("name",nestedObject.optString("name"));
                    editor.putString("email",nestedObject.optString("email"));
                    editor.putString("contact",nestedObject.optString("contact"));
                    editor.putString("address",nestedObject.optString("address"));
                    editor.putString("user_type",nestedObject.optString("user_type"));
                    editor.apply();

                    startActivity(new Intent(SignIn.this,
                            Dashboard.class));
                    finish();

                } else {

                    Snackbar.make(findViewById(android.R.id.content), "Login Failed !", Snackbar.LENGTH_LONG)
                            .setAction("OK", null).show();


                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            super.onPostExecute(aVoid);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATTION) {
        } else {
        }
    }
}

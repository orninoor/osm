package com.nafis.osm.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nafis.osm.R;
import com.nafis.osm.Utility.StringHolder;
import com.nafis.osm.Utility.Utility;
import com.nafis.osm.helper.GPSTracker;
import com.nafis.osm.model.parkingModel;

import org.json.JSONArray;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.nafis.osm.Utility.StringHolder.AllParkingData;

public class ApproveParking extends AppCompatActivity {

    IMapController mapController;
    MapView map;
    ArrayList<parkingModel> allData = new ArrayList<>();
    FloatingActionButton add_point, reload;
    double $_latitude = 0.0, $_longitude = 0.0;
    String markerID = "";
    Toolbar mytoolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        setContentView(R.layout.landing_page);

        Initialization();
        setToolBar();
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        mapController = map.getController();
        mapController.setZoom(16.0);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            return;
        }


        GPSTracker gps = new GPSTracker(this);
        if (gps.canGetLocation()) {

            $_latitude = gps.getLatitude();
            $_longitude = gps.getLongitude();


            CurrentLocationPin($_latitude, $_longitude);

            Log.e("Location ===> ", gps.getLatitude() + "," + gps.getLongitude());

        }

        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(ApproveParking.this, ApproveParking.class));
            }
        });


        add_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                startActivity(new Intent(ApproveParking.this, AddParking.class));


            }
        });


        new ParseAllParkingData().execute();

    }

    private void Initialization() {
        map = (MapView) findViewById(R.id.map);
        add_point = (FloatingActionButton) findViewById(R.id.add_marker);
        reload = (FloatingActionButton) findViewById(R.id.reload);
    }


    /*
     *
     * Parameter :  latitude , longitude
     * Return : Set the Initial pin on Current location
     *
     * */

    private void CurrentLocationPin(double $_latitude, double $_longitude) {
        final GeoPoint geoPoint = new GeoPoint($_latitude, $_longitude);
        mapController.setCenter(geoPoint);
        OverlayItem overlayItem = new OverlayItem("Parking Space", "", geoPoint);
        Drawable markerDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_current_position);
        overlayItem.setMarker(markerDrawable);

        ArrayList<OverlayItem> overlayItemArrayList = new ArrayList<>();
        overlayItemArrayList.add(overlayItem);

        ItemizedOverlay<OverlayItem> locationOverlay = new ItemizedIconOverlay<>(overlayItemArrayList, new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
            @Override
            public boolean onItemSingleTapUp(int i, OverlayItem overlayItem) {
                return true;


            }

            @Override
            public boolean onItemLongPress(int i, OverlayItem overlayItem) {
                return false;
            }
        }, getApplicationContext());


        map.getOverlays().add(locationOverlay);
    }


    /*
     *
     Asyntask for all parking points
     *
     * */

    private class ParseAllParkingData extends AsyncTask<String, Void, Void> {

        String JsonString;

        @Override
        protected Void doInBackground(String... strings) {

            try {

                JsonString = Utility.run(AllParkingData);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                ParseJsonData(JsonString);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
     *
     * Parse all JSON data
     *
     * */

    private void ParseJsonData(String jsonString) throws Exception {


        allData.clear();

        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray jsonArray = jsonObject.getJSONArray("parking");


        for (int i = 0; i < jsonArray.length(); i++) {

            parkingModel model = new parkingModel();
            model.setAddress(jsonArray.getJSONObject(i).optString("address"));
            model.setDescription(jsonArray.getJSONObject(i).optString("Description"));
            model.setHourly_rate(jsonArray.getJSONObject(i).optString("hourly_rate"));
            model.setLati(jsonArray.getJSONObject(i).optString("lati"));
            model.setLocation_name(jsonArray.getJSONObject(i).optString("location_name"));
            model.setLongi(jsonArray.getJSONObject(i).optString("longi"));
            model.setOthers(jsonArray.getJSONObject(i).optString("lati"));
            model.setRating(jsonArray.getJSONObject(i).optString("longi"));
            model.setUserID(jsonArray.getJSONObject(i).optString("userID"));
            model.setID(jsonArray.getJSONObject(i).optString("id"));
            model.setOpening_time(jsonArray.getJSONObject(i).optString("opening_time"));
            model.setClosing_time(jsonArray.getJSONObject(i).optString("closing_time"));
            model.setCapacity(jsonArray.getJSONObject(i).optString("capacity"));
            model.setOccupied(jsonArray.getJSONObject(i).optString("occupied"));


            if (jsonArray.getJSONObject(i).optString("approved").contentEquals("false")) {

                allData.add(model);
            }


            SetupPininMap(allData);

        }

        Log.e("Size of Data Array", "" + allData.size());

    }

    /*
     *
     * Setup pin in map from Parking data
     *
     * */
    private void SetupPininMap(final ArrayList<parkingModel> allData) {


        final ArrayList<OverlayItem> items = new ArrayList<OverlayItem>();

        for (int i = 0; i < allData.size(); i++) {

            GeoPoint currentLocation = new GeoPoint(Double.parseDouble(allData.get(i).getLati()), Double.parseDouble(allData.get(i).getLongi()));

            OverlayItem myLocationOverlayItem = new OverlayItem(allData.get(i).getLocation_name(), allData.get(i).getDescription(), currentLocation);


            Drawable myCurrentLocationMarker = this.getResources().getDrawable(R.drawable.ic_cl_pin);

            myLocationOverlayItem.setMarker(myCurrentLocationMarker);
            items.add(myLocationOverlayItem);

        }


        ItemizedOverlay<OverlayItem> locationOverlay = new ItemizedIconOverlay<>(items, new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
            @Override
            public boolean onItemSingleTapUp(int i, OverlayItem overlayItem) {


                Log.e("***** TAP ****", " *** TAPPY ***");

                parkingModel model = allData.get(i);

                CreateDialogue(model);

                markerID = model.getID();

                return true;
            }

            @Override
            public boolean onItemLongPress(int i, OverlayItem overlayItem) {
                return false;
            }
        }, getApplicationContext());

        this.map.getOverlays().add(locationOverlay);
        map.getOverlays().add(locationOverlay);


    }


    private void CreateDialogue(parkingModel model) {

        LayoutInflater factory = LayoutInflater.from(this);
        final View approvalDialogView = factory.inflate(R.layout.approve_parking, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(approvalDialogView);


        TextView parking_name = approvalDialogView.findViewById(R.id.parking_name);
        TextView parking_total = approvalDialogView.findViewById(R.id.parking_total);
        TextView parking_free = approvalDialogView.findViewById(R.id.parking_free);
        TextView parking_time = approvalDialogView.findViewById(R.id.parking_time);
        TextView parking_rate = approvalDialogView.findViewById(R.id.parking_rate);
        TextView parking_address = approvalDialogView.findViewById(R.id.parking_address);

        parking_name.setText("Name " + model.getLocation_name());
        parking_total.setText("Capacity " + model.getCapacity());
        parking_free.setText("Available : " + model.getOccupied());
        parking_time.setText("Time : " + model.getOpening_time() + "-" + model.getClosing_time());
        parking_rate.setText("Cost/Hour : " + model.getHourly_rate());
        parking_address.setText("Address : " + model.getAddress());

        final Button approve = approvalDialogView.findViewById(R.id.approve_button);
        final Button decline = approvalDialogView.findViewById(R.id.decline_button);

        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteDialog.cancel();
                new ApproveParkingAsynTask().execute("true");

            }
        });


        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteDialog.cancel();
                new ApproveParkingAsynTask().execute("false");
            }
        });

        deleteDialog.show();
    }

    private class ApproveParkingAsynTask extends AsyncTask<String, Void, Void> {

        String JsonResponse = "";

        @Override
        protected Void doInBackground(String... strings) {

            try {


                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id", markerID);
                jsonObject.put("approved", strings[0]);

                JsonResponse = Utility.post(StringHolder.approveStatus, "" + jsonObject);


            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {

                JSONObject jsonObject = new JSONObject(JsonResponse);

                if (jsonObject.optInt("status") == 200) {

                    new SweetAlertDialog(ApproveParking.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Approved")
                            .setContentText("The parking is Added ")
                            .show();

                } else {


                    new SweetAlertDialog(ApproveParking.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Something went wrong!")
                            .show();
                    finish();
                    startActivity(new Intent(ApproveParking.this, ApproveParking.class));

                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    private void setToolBar() {
        mytoolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Approve Parking ");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }


}

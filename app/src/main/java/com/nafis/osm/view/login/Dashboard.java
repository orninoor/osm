package com.nafis.osm.view.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.nafis.osm.R;
import com.nafis.osm.view.activity.AddParking;
import com.nafis.osm.view.activity.AllParking;
import com.nafis.osm.view.activity.ApproveParking;
import com.nafis.osm.view.activity.EditProfile;

import de.hdodenhof.circleimageview.CircleImageView;

public class Dashboard extends AppCompatActivity {

    CircleImageView user_Logo;
    LinearLayout showParking, pendingApproval, addParking, editprofile, logout_view;
    TextView name_of_user, user_contact, user_email;
    SharedPreferences prefs;
    String user_role;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_dashboard_);

        Initialization();
        SetUserData();
    }

    private void SetUserData() {

        name_of_user.setText("NAME : " + prefs.getString("name", "name"));
        user_contact.setText("PHONE : " + prefs.getString("contact", "contact"));
        user_email.setText("EMAIL : " + prefs.getString("email", "email"));

        user_role = prefs.getString("user_type", "0");
    }

    private void Initialization() {

        user_Logo = (CircleImageView) findViewById(R.id.user_Logo);

        prefs = getSharedPreferences("UserData", MODE_PRIVATE);

        showParking = findViewById(R.id._menu_show_parking);
        pendingApproval = findViewById(R.id._menu_approve_pending_parking);
        addParking = findViewById(R.id._menu_add_parking);
        editprofile = findViewById(R.id._menu_edit_profile);
        logout_view = findViewById(R.id.logout_view);

        name_of_user = findViewById(R.id.name_of_user);
        user_contact = findViewById(R.id.user_contact);
        user_email = findViewById(R.id.user_email);


        showParking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Dashboard.this,
                        AllParking.class));

            }
        });

        pendingApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user_role.contentEquals("1")) {
                    startActivity(new Intent(Dashboard.this,
                            ApproveParking.class));
                } else {

                    PopulateSankbar(Dashboard.this, "To Add parking , need to Login as Admin !! ");
                }


            }
        });


        addParking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Dashboard.this, AddParking.class));

            }
        });

        logout_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logout();
                finish();

            }
        });

        editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, EditProfile.class));

            }
        });
    }

    private void Logout() {
        SharedPreferences settings = getApplicationContext()
                .getSharedPreferences("UserData", Context.MODE_PRIVATE);
        settings.edit().clear().commit();
    }

    public static void PopulateSankbar(Activity activity, String s) {

        Snackbar.make(activity.findViewById(android.R.id.content), s, Snackbar.LENGTH_LONG)
                .setTextColor(activity.getResources().getColor(R.color.white))
                .setBackgroundTint(activity.getResources().getColor(R.color.colorPrimary))
                .show();
    }
}

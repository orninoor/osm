package com.nafis.osm.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nafis.osm.R;
import com.nafis.osm.Utility.StringHolder;
import com.nafis.osm.Utility.Utility;
import com.nafis.osm.helper.GPSTracker;
import com.nafis.osm.model.parkingModel;

import org.json.JSONArray;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.bonuspack.routing.MapQuestRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.bonuspack.routing.RoadNode;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.Polyline;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.nafis.osm.Utility.StringHolder.AllParkingData;

public class AllParking extends AppCompatActivity {

    IMapController mapController;
    MapView map;
    ArrayList<parkingModel> allData = new ArrayList<>();
    FloatingActionButton add_point, reload;
    double $_latitude = 0.0, $_longitude = 0.0;
    int mIncr = 10000;
    Toolbar mytoolbar;
    String Time = "", ParkingHourlyRate = "", ParkingAddress = "", OwnersName = "", OwnersMobile = "", ParkingContact = "";

    ArrayList<GeoPoint> waypoints;
    Marker CurrentMarker;
    Polyline roadOverlay;
    GeoPoint geoPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        setContentView(R.layout.landing_page);

        Initialization();
        setToolBar();

        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        mapController = map.getController();
        mapController.setZoom(16.0);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            return;
        }//Source code


        GPSTracker gps = new GPSTracker(this);
        if (gps.canGetLocation()) {

            $_latitude = gps.getLatitude();
            $_longitude = gps.getLongitude();


            CurrentLocationPin($_latitude, $_longitude);

            Log.e("Location ===> ", gps.getLatitude() + "," + gps.getLongitude());

        }

        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(AllParking.this, AllParking.class));
            }
        });


        add_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                startActivity(new Intent(AllParking.this, AddParking.class));


            }
        });


        new ParseAllParkingData().execute();

    }

    private void Initialization() {
        map = (MapView) findViewById(R.id.map);
        add_point = (FloatingActionButton) findViewById(R.id.add_marker);
        reload = (FloatingActionButton) findViewById(R.id.reload);
        waypoints = new ArrayList<GeoPoint>();
    }


    /*
     *
     * Parameter :  latitude , longitude
     * Return : Set the Initial pin on Current location
     *
     * */

    private void CurrentLocationPin(double $_latitude, double $_longitude) {
        geoPoint = new GeoPoint($_latitude, $_longitude);
        mapController.setCenter(geoPoint);
        OverlayItem overlayItem = new OverlayItem("Parking Space", "", geoPoint);
        Drawable markerDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_current_position);
        overlayItem.setMarker(markerDrawable);

        ArrayList<OverlayItem> overlayItemArrayList = new ArrayList<>();
        overlayItemArrayList.add(overlayItem);

        ItemizedOverlay<OverlayItem> locationOverlay = new ItemizedIconOverlay<>(overlayItemArrayList, new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
            @Override
            public boolean onItemSingleTapUp(int i, OverlayItem overlayItem) {
                return true;
            }

            @Override
            public boolean onItemLongPress(int i, OverlayItem overlayItem) {
                return false;
            }
        }, getApplicationContext());


        map.getOverlays().add(locationOverlay);
    }


    /*
     *
     Asyntask for all parking points
     *
     * */

    private class ParseAllParkingData extends AsyncTask<String, Void, Void> {

        String JsonString;

        @Override
        protected Void doInBackground(String... strings) {

            try {

                JsonString = Utility.run(AllParkingData);


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                ParseJsonData(JsonString);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
     *
     * Parse all JSON data
     *
     * */

    private void ParseJsonData(String jsonString) throws Exception {


        allData.clear();

        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray jsonArray = jsonObject.getJSONArray("parking");


        for (int i = 0; i < jsonArray.length(); i++) {

            parkingModel model = new parkingModel();
            model.setID(jsonArray.getJSONObject(i).optString("id"));
            model.setLocation_name(jsonArray.getJSONObject(i).optString("location_name"));
            model.setLati(jsonArray.getJSONObject(i).optString("lati"));
            model.setLongi(jsonArray.getJSONObject(i).optString("longi"));
            model.setDescription(jsonArray.getJSONObject(i).optString("Description"));
            model.setHourly_rate(jsonArray.getJSONObject(i).optString("hourly_rate"));
            model.setAddress(jsonArray.getJSONObject(i).optString("address"));
            model.setReg_id(jsonArray.getJSONObject(i).optString("reg_id"));
            model.setCapacity(jsonArray.getJSONObject(i).optString("capacity"));
            model.setOccupied(jsonArray.getJSONObject(i).optString("occupied"));
            model.setOpening_time(jsonArray.getJSONObject(i).optString("opening_time"));
            model.setClosing_time(jsonArray.getJSONObject(i).optString("closing_time"));
            model.setContact(jsonArray.getJSONObject(i).optString("contact"));
            model.setApproved(jsonArray.getJSONObject(i).optString("approved"));

            allData.add(model);

            SetupPininMap(allData);

        }

        Log.e("Size of Data Array", "" + allData.size());

    }

    /*
     *
     * Setup pin in map from Parking data
     *
     * */
    private void SetupPininMap(final ArrayList<parkingModel> allData) {


        final ArrayList<OverlayItem> items = new ArrayList<OverlayItem>();

        for (int i = 0; i < allData.size(); i++) {

            GeoPoint currentLocation = new GeoPoint(Double.parseDouble(allData.get(i).getLati()), Double.parseDouble(allData.get(i).getLongi()));

            OverlayItem myLocationOverlayItem = new OverlayItem(allData.get(i).getLocation_name(), allData.get(i).getDescription(), currentLocation);


            if (allData.get(i).getApproved().toString().contentEquals("false")) {
                Drawable myCurrentLocationMarker = this.getResources().getDrawable(R.drawable.ic_cl_pin);
                myLocationOverlayItem.setMarker(myCurrentLocationMarker);
                items.add(myLocationOverlayItem);

            } else {

                Drawable myCurrentLocationMarker = this.getResources().getDrawable(R.drawable.ic_cl_pin_approved);
                myLocationOverlayItem.setMarker(myCurrentLocationMarker);
                items.add(myLocationOverlayItem);

            }


        }


        ItemizedOverlay<OverlayItem> locationOverlay = new ItemizedIconOverlay<>(items, new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
            @Override
            public boolean onItemSingleTapUp(int i, OverlayItem overlayItem) {


                Log.e("***** TAP ****", " *** TAPPY ***");

                parkingModel model = allData.get(i);

                CreateDialogue(model, i);

                return true;
            }

            @Override
            public boolean onItemLongPress(int i, OverlayItem overlayItem) {

                try {

                    //Driving Path
                    waypoints = new ArrayList<GeoPoint>();
                    waypoints.add(geoPoint);
                    GeoPoint endPoint = new GeoPoint(Double.parseDouble(allData.get(i).getLati()), Double.parseDouble(allData.get(i).getLongi()));
                    waypoints.add(endPoint);

                    new DrivingPath().execute();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        }, getApplicationContext());

        this.map.getOverlays().add(locationOverlay);
        map.getOverlays().add(locationOverlay);


    }

    private class DrivingPath extends AsyncTask<String, Void, Void> {
        @SuppressLint("WrongThread")
        @Override
        protected Void doInBackground(String... strings) {

            try {
                RoadManager roadManager = new MapQuestRoadManager("YibEKKkT4bGYtRlGHqGjiuxkYYNHDPXb");
                Road road = roadManager.getRoad(waypoints);
                roadOverlay = RoadManager.buildRoadOverlay(road);

                Drawable nodeIcon = getResources().getDrawable(R.drawable.ic_panels);
                for (int i = 0; i < road.mNodes.size(); i++) {
                    RoadNode node = road.mNodes.get(i);
                    Marker nodeMarker = new Marker(map);
                    nodeMarker.setPosition(node.mLocation);
                    nodeMarker.setSnippet(node.mInstructions);
                    nodeMarker.setSubDescription(Road.getLengthDurationText(AllParking.this, node.mLength, node.mDuration));
                    nodeMarker.setIcon(nodeIcon);
                    Drawable icon = getResources().getDrawable(R.drawable.ic_turn_right);
                    nodeMarker.setImage(icon);
                    map.getOverlays().add(nodeMarker);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            map.getOverlays().add(roadOverlay);
            map.invalidate();
        }
    }


    private void CreateDialogue(final parkingModel model, int i) {

        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.show_parking, null);
        final AlertDialog parkingDetailsDialogue = new AlertDialog.Builder(this).create();
        parkingDetailsDialogue.setView(deleteDialogView);


        final TextView parking_name = deleteDialogView.findViewById(R.id.parking_name);
        final TextView parking_total = deleteDialogView.findViewById(R.id.parking_total);
        final TextView parking_free = deleteDialogView.findViewById(R.id.parking_free);
        final TextView parking_time = deleteDialogView.findViewById(R.id.parking_time);
        final TextView parking_rate = deleteDialogView.findViewById(R.id.parking_rate);
        final TextView parking_address = deleteDialogView.findViewById(R.id.parking_address);
        final ImageView approval_status = deleteDialogView.findViewById(R.id.parking_approval);
        final Button bookmycar = deleteDialogView.findViewById(R.id.bookmycar);
        final ImageView call_for_parking = deleteDialogView.findViewById(R.id.call_for_parking);
        final ImageView parkdestination = deleteDialogView.findViewById(R.id.parkdestination);

        parking_name.setText("Parking : " + model.getLocation_name());
        parking_total.setText("Capacity : " + model.getCapacity());
        parking_free.setText("Vacancy : " + model.getOccupied());
        parking_time.setText("Timing : " + model.getOpening_time() + " - " + model.getClosing_time());
        parking_rate.setText("Hourly rate : " + model.getHourly_rate());
        parking_address.setText("Address : " + model.getAddress());


        //TODO

        ParkingHourlyRate = model.getHourly_rate();
        ParkingAddress = model.getAddress();
        OwnersName = model.getLocation_name();
        ParkingContact = model.getContact();


        if (model.getApproved().toString().contentEquals("false")) {
            approval_status.setImageResource(R.drawable.approved_);
            approval_status.setAlpha((float) 0.75);

        } else {

            approval_status.setImageResource(R.drawable.approve_q);
            approval_status.setAlpha((float) 0.5);
        }

        bookmycar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (model.getCapacity().contentEquals(model.getOccupied())) {


                } else {

                }

                if (model.getApproved().toString().contentEquals("true")) {

                    parkingDetailsDialogue.cancel();
                    new BookParking().execute(model.getID());


                } else {

                    parkingDetailsDialogue.cancel();

                    new SweetAlertDialog(AllParking.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Parking is not approved !")
                            .show();
                }

            }
        });

        call_for_parking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.getApproved().toString().contentEquals("true")) {

                    parkingDetailsDialogue.cancel();

                    Utility.DialCall(AllParking.this, model.getContact());

                } else {

                    parkingDetailsDialogue.cancel();

                    new SweetAlertDialog(AllParking.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Parking is not approved !")
                            .show();

                }
            }
        });

        parkdestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Driving Path
                waypoints = new ArrayList<GeoPoint>();
                waypoints.add(geoPoint);
                GeoPoint endPoint = new GeoPoint(Double.parseDouble(allData.get(i).getLati()), Double.parseDouble(allData.get(i).getLongi()));
                waypoints.add(endPoint);

                new DrivingPath().execute();

                parkingDetailsDialogue.cancel();
            }
        });


        parkingDetailsDialogue.show();
    }


    private class BookParking extends AsyncTask<String, Void, Void> {
        String JsonResponse = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id", strings[0]);
                JsonResponse = Utility.post(StringHolder.bookAParking, "" + jsonObject);
                Log.e("Book Parking", JsonResponse);

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                JSONObject jsonObject = new JSONObject(JsonResponse);
                if (jsonObject.optInt("status") == 200) {
                    new SweetAlertDialog(AllParking.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Successfully Booked !")
                            .setConfirmText("Park & collect ticket . ")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();

                                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                                    Time = dateFormat.format(new Date());

                                    startActivity(new Intent(AllParking.this, ParkingTicket.class)
                                            .putExtra("time", Time)
                                            .putExtra("cost", ParkingHourlyRate)
                                            .putExtra("name", OwnersName)
                                            .putExtra("mobile", ParkingContact)
                                    );
                                }
                            })
                            .show();
                } else {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void setToolBar() {
        mytoolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("All Parking ");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }


}

package com.nafis.osm.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.nafis.osm.R;
import com.nafis.osm.Utility.StringHolder;
import com.nafis.osm.Utility.Utility;
import com.nafis.osm.helper.GPSTracker;

import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.text.DecimalFormat;
import java.util.ArrayList;


public class AddParking extends AppCompatActivity {
    MapView map = null;
    IMapController mapController;
    double $_latitude = 0.0, $_longitude = 0.0;
    FloatingActionButton map_view;
    Toolbar mytoolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        setContentView(R.layout.add_parking_);
        setToolBar();

        map_view = (FloatingActionButton) findViewById(R.id.map_view);
        map = (MapView) findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        mapController = map.getController();
        mapController.setZoom(16.0);

        map_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                startActivity(new Intent(AddParking.this, AllParking.class));
            }
        });


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            return;
        }


        GPSTracker gps = new GPSTracker(this);
        if (gps.canGetLocation()) {

            $_latitude = gps.getLatitude();
            $_longitude = gps.getLongitude();


            CurrentLocationPin($_latitude, $_longitude);

            Log.e("Location ===> ", gps.getLatitude() + "," + gps.getLongitude());

        }


        final MapEventsReceiver mReceive = new MapEventsReceiver() {
            @Override
            public boolean singleTapConfirmedHelper(GeoPoint p) {

                Log.e("Tapped position", p.getLatitude() + "  ,  " + p.getLongitude());

                AddParkingMarker(p.getLatitude(), p.getLongitude());

                return false;
            }

            @Override
            public boolean longPressHelper(GeoPoint p) {


                return false;
            }
        };
        map.getOverlays().add(new MapEventsOverlay(mReceive));


    }

    /*
     *
     * Parameter :  latitude , longitude
     * Return : Set the Initial pin on Current location
     *
     * */

    private void CurrentLocationPin(double $_latitude, double $_longitude) {
        final GeoPoint geoPoint = new GeoPoint($_latitude, $_longitude);
        mapController.setCenter(geoPoint);
        OverlayItem overlayItem = new OverlayItem("Parking Space", "", geoPoint);
        Drawable markerDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pointer);
        overlayItem.setMarker(markerDrawable);

        ArrayList<OverlayItem> overlayItemArrayList = new ArrayList<>();
        overlayItemArrayList.add(overlayItem);

        ItemizedOverlay<OverlayItem> locationOverlay = new ItemizedIconOverlay<>(overlayItemArrayList, new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
            @Override
            public boolean onItemSingleTapUp(int i, OverlayItem overlayItem) {


                return true;
            }

            @Override
            public boolean onItemLongPress(int i, OverlayItem overlayItem) {

                Log.e("***** LONG TAP ****", " *** TAPPY LONG ***");
                return false;
            }
        }, getApplicationContext());


        map.getOverlays().add(locationOverlay);
    }

    /*
     *
     * Parameter : latitude , longitude
     * Return : Add marker on Tap in Map
     *
     * */


    private void AddParkingMarker(double latitude, double longitude) {

        String map_point = new DecimalFormat("##.####").format(latitude) + "  ,  " + new DecimalFormat("##.####").format(longitude);

        final GeoPoint geoPoint = new GeoPoint(latitude, longitude);
        mapController.setCenter(geoPoint);
        OverlayItem overlayItem = new OverlayItem("Parking Space", map_point, geoPoint);
        Drawable markerDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_parking_);
        overlayItem.setMarker(markerDrawable);

        ArrayList<OverlayItem> overlayItemArrayList = new ArrayList<>();
        overlayItemArrayList.add(overlayItem);

        ItemizedOverlay<OverlayItem> locationOverlay = new ItemizedIconOverlay<>(overlayItemArrayList, new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
            @Override
            public boolean onItemSingleTapUp(int i, OverlayItem overlayItem) {

                CreateDialogue(geoPoint);
                return true;
            }

            @Override
            public boolean onItemLongPress(int i, OverlayItem overlayItem) {
                return false;
            }
        }, getApplicationContext());

        this.map.getOverlays().add(locationOverlay);
        map.getOverlays().add(locationOverlay);
    }


    /*
     *
     * Parameter : GeoPoint
     * Return : Dialogue Interface for Parking Add
     *
     * */

    private void CreateDialogue(final GeoPoint p) {

        LayoutInflater factory = LayoutInflater.from(this);
        final View deleteDialogView = factory.inflate(R.layout.add_parking_form, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        deleteDialog.setView(deleteDialogView);

        final EditText coordinate = deleteDialogView.findViewById(R.id.coordinate);
        final EditText location = deleteDialogView.findViewById(R.id.location_name);
        final EditText description = deleteDialogView.findViewById(R.id.description);
        final EditText address = deleteDialogView.findViewById(R.id.full_address);
        final EditText total_number = deleteDialogView.findViewById(R.id.total_number);
        final EditText booked_status = deleteDialogView.findViewById(R.id.booked_status);
        final EditText parking_start = deleteDialogView.findViewById(R.id.parking_start);
        final EditText parking_end = deleteDialogView.findViewById(R.id.parking_end);
        final EditText hourly_rate = deleteDialogView.findViewById(R.id.hourly_rate);
        final EditText contact_of_owner = deleteDialogView.findViewById(R.id.contact_of_owner);
        final CheckBox sat = deleteDialogView.findViewById(R.id.sat_btn);
        final CheckBox sun = deleteDialogView.findViewById(R.id.sun_btn);
        final CheckBox mon = deleteDialogView.findViewById(R.id.mon_btn);
        final CheckBox tues = deleteDialogView.findViewById(R.id.tues_btn);
        final CheckBox wed = deleteDialogView.findViewById(R.id.wed_btn);
        final CheckBox thurs = deleteDialogView.findViewById(R.id.thurs_btn);
        final CheckBox fri = deleteDialogView.findViewById(R.id.fri_fri);


        coordinate.setText(new DecimalFormat("##.####").format(p.getLatitude()) + "  ,  " + new DecimalFormat("##.####").format(p.getLongitude()));

        final double lati = p.getLatitude();
        final double longi = p.getLongitude();

        deleteDialogView.findViewById(R.id.submitdata).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String location_ = location.getText().toString();
                String description_ = description.getText().toString();
                String address_ = address.getText().toString();
                int total_number_ = Integer.parseInt(total_number.getText().toString());
                int booked_status_ = Integer.parseInt(booked_status.getText().toString());
                String parking_start_ = parking_start.getText().toString();
                String parking_end_ = parking_end.getText().toString();
                String hourly_rate_ = hourly_rate.getText().toString();
                String contact_of_owner_ = contact_of_owner.getText().toString();
                String sat_status = "" + sat.isChecked();
                String sun_status = "" + sun.isChecked();
                String mon_status = "" + mon.isChecked();
                String tues_status = "" + tues.isChecked();
                String wed_status = "" + wed.isChecked();
                String thurs_status = "" + thurs.isChecked();
                String fri_status = "" + fri.isChecked();


                if (!location_.contentEquals("")) {


                    if (!(total_number.getText().toString().contentEquals("") ||
                            booked_status.getText().toString().contentEquals(""))) {


                        if (!(parking_start_.contentEquals("") ||
                                parking_end_.contentEquals(""))) {


                            if (!hourly_rate_.contentEquals("")) {


                                if (!address_.contentEquals("")) {


                                    if (!contact_of_owner_.contentEquals("")) {


                                        PrepareDataForSync(sat_status, sun_status, mon_status,
                                                tues_status, wed_status, location_, thurs_status,
                                                fri_status, description_, address_, lati, longi,
                                                deleteDialog, total_number_, booked_status_,
                                                parking_start_, parking_end_, hourly_rate_,
                                                contact_of_owner_);


                                    }else {

                                        Snackbar.make(v, "Contact name is blank .", Snackbar.LENGTH_LONG)
                                                .setAction("OK", null).show();
                                    }

                                }else {

                                    Snackbar.make(v, "Address name is blank .", Snackbar.LENGTH_LONG)
                                            .setAction("OK", null).show();
                                }

                            }else {

                                Snackbar.make(v, "Hourly rate name is blank .", Snackbar.LENGTH_LONG)
                                        .setAction("OK", null).show();
                            }

                        } else {

                            Snackbar.make(v, "Available hour is blank .", Snackbar.LENGTH_LONG)
                                    .setAction("OK", null).show();
                        }

                    } else {

                        Snackbar.make(v, "Parking Information is blank .", Snackbar.LENGTH_LONG)
                                .setAction("OK", null).show();
                    }

                } else {

                    Snackbar.make(v, "Location name is blank .", Snackbar.LENGTH_LONG)
                            .setAction("OK", null).show();
                }


            }
        });


        deleteDialog.show();
    }


    /*
     *
     * Parameter : location , description , address , latitude , longitude
     * Return : Prepare JSON for Sync
     *
     * */
    private void PrepareDataForSync(String sat_status, String sun_status, String mon_status,
                                    String tues_status, String wed_status, String location_,
                                    String thurs_status, String fri_status, String description_,
                                    String address_, double lati, double longi, AlertDialog deleteDialog,
                                    int total_number_, int booked_status_, String parking_start_,
                                    String parking_end_, String hourly_rate_, String contact_of_owner_) {


        try {

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("reg_id", "22");
            jsonObject.put("location_name", location_);
            jsonObject.put("lati", "" + lati);
            jsonObject.put("longi", "" + longi);
            jsonObject.put("Description", description_);
            jsonObject.put("address", address_);
            jsonObject.put("hourly_rate", hourly_rate_);
            jsonObject.put("capacity", total_number_);
            jsonObject.put("occupied", 0);
            jsonObject.put("opening_time", parking_start_);
            jsonObject.put("closing_time", parking_end_);
            jsonObject.put("cost", "");
            jsonObject.put("contact", contact_of_owner_);
            jsonObject.put("sat", sat_status);
            jsonObject.put("sun", sun_status);
            jsonObject.put("mon", mon_status);
            jsonObject.put("tue", tues_status);
            jsonObject.put("wed", wed_status);
            jsonObject.put("thu", thurs_status);
            jsonObject.put("fri", fri_status);
            jsonObject.put("approved", "false");

            Log.e("SEND *****> ", "" + jsonObject);

            deleteDialog.dismiss();

            new SendDatatoNMapServer().execute("" + jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     *
     * Parameter : JSON String
     * Return : Send the data to server
     *
     * */

    private class SendDatatoNMapServer extends AsyncTask<String, Void, Void> {

        String jsonString;

        @Override
        protected Void doInBackground(String... strings) {

            try {

                jsonString = Utility.post(StringHolder.SendData, strings[0]);
                Log.e("SEND REPLY --> ", jsonString);

            } catch (Exception e) {
                e.printStackTrace();
                //
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(getApplicationContext(), jsonString, Toast.LENGTH_LONG).show();
        }
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();

    }

    private void setToolBar() {
        mytoolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Parking ");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }


}
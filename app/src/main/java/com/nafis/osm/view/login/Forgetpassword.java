package com.nafis.osm.view.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.MenuItem;

import com.nafis.osm.R;

public class Forgetpassword extends AppCompatActivity {

    Toolbar mytoolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpassword);

        setToolBar();
    }

    private void setToolBar() {
        mytoolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Forget Password ");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }
}
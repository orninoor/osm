package com.nafis.osm.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.nafis.osm.R;

public class EditProfile extends AppCompatActivity {

    EditText name, email, phone, address;
    SharedPreferences prefs;
    Toolbar mytoolbar;
    Button update_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Initialization();
        setToolBar();
        SetValueToField();

        update_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void SetValueToField() {

        name.setText(prefs.getString("name", "NA"));
        email.setText(prefs.getString("email", "NA"));
        phone.setText(prefs.getString("contact", "NA"));
        address.setText(prefs.getString("address", "NA"));
    }

    private void Initialization() {

        prefs = getSharedPreferences("UserData", MODE_PRIVATE);
        update_data=findViewById(R.id.update_data);
        name = (EditText) findViewById(R.id.et_name);
        email = (EditText) findViewById(R.id.et_email);
        phone = (EditText) findViewById(R.id.et_phone);
        address = (EditText) findViewById(R.id.et_address);
    }
    private void setToolBar() {
        mytoolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit Profile");


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:

                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }

    }
}